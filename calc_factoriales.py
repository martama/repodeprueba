#Creamos un programa que calcule los factoriales de los números entre 1 y 10 (incluidos)

def calc_factorial(num):
    factorial = 1
    for i in range(1,num+1):
        factorial = i * factorial
    return factorial
#Calculamos los factoriales de los números del 1-10 y los mostramos por pantalla
for i in range (1, 11):
    factorial = calc_factorial(i)
    print(f"El factorial de {i} es -> {factorial}")



